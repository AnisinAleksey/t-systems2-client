package com.tsystems.logiweb.driver.controller;

import com.tsystems.logiweb.driver.interceptor.ExceptionControllerHandler;
import com.tsystems.logiweb.driver.service.LoginService;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.StreamingOutput;

@Path("/login")
@ExceptionControllerHandler
public class LoginController extends ApplicationController {

    @Inject
    private LoginService loginService;

    @POST
    @Consumes("application/json")
    public void login(JsonObject jsonObject) throws Exception {
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        loginService.login(username,password);
    }

    @GET
    public StreamingOutput page() {
        return resource("/login.html");
    }
}
