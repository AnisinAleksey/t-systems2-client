package com.tsystems.logiweb.driver.auth;


import javax.ejb.Stateful;

@Stateful
public class AuthenticationBean {
    
    private String jSessionID;

    public String getJSessionID() {
        return jSessionID;
    }

    public void setJSessionID(String jSessionID) {
        this.jSessionID = jSessionID;
    }

    public void clearSession() {
        this.jSessionID = null;
    }
}
