export default `.loginForm {
    position: absolute;
    top: 40%;
    left: 40%;
}

.headerSpan {
    margin-left: 28%;
    font-size: 40px;
    color: #a267fd;
    font-weight: bolder;
}

.fieldDiv {
    margin: 5px;
}

.loginButtonClass{
    font-size: 18px;
    width: 120px;
    height: 50px;
    font-weight: bold;
    padding: 5px;
    color: #FFFFFF;
    background-color: #8b7fec;
    margin-left: 24%;
    border-radius: 8px;
}

.loginField {
    height: 20px;
    width: 250px;
    border-radius: 10px;
    border-color: #cfc3ec;
    padding: 10px;
    color: #892fcc;
    font-weight: bold;
}

input {outline: none;}
input:-webkit-autofill {
    -webkit-box-shadow: inset 0 0 0 50px #fff !important; /* Цвет фона */
    -webkit-text-fill-color: #999 !important; /* цвет текста */
    color: #999 !important; /* цвет текста */
}

button[type="submit"]:disabled {
    background-color: #9e9299;
}

.hiddenSpan{
    visibility:hidden;
    color: red;
    margin-left: 10%;
    font-weight: bolder;
}
`;