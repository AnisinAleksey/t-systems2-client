package com.tsystems.logiweb.driver.constant;


public class AppConstants {
    public static final String EXTERNAL_HOST = "localhost";
    public static final int EXTERNAL_PORT = 8080;
    public static final String JSESSIONID= "JSESSIONID";
}
