"use strict";
require('zone.js/dist/zone');
require('reflect-metadata');
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var http_1 = require('@angular/http');
var login_component_1 = require('./app/components/login/login.component');
var login_service_1 = require("./app/service/login-service");
platform_browser_dynamic_1.bootstrap(login_component_1.Login, [login_service_1.LoginService, http_1.HTTP_BINDINGS]);
//# sourceMappingURL=login.js.map