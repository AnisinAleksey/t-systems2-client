import 'zone.js/dist/zone';
import 'reflect-metadata';
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { HTTP_BINDINGS }    from '@angular/http';
import { Login } from './app/components/login/login.component';
import {LoginService} from "./app/service/login-service"

bootstrap(Login, [LoginService,HTTP_BINDINGS]);