'use strict';

// Depends
var path         = require('path');
var TextPlugin   = require('extract-text-webpack-plugin');
var HtmlPlugin   = require('html-webpack-plugin');

/**
 * Global webpack config
 * @param  {[type]} _path [description]
 * @return {[type]}       [description]
 */

let extractCSS = new TextPlugin('css/[name].css',{allChunks: true});

module.exports = function(_path) {
    // return objecy
    return {
        // entry points
        entry: {
            login : path.join(_path,'src','login')
        },

        // output system
        output: {
            path: path.join(_path, 'target'),
            filename: 'js/[name].js'
        },

        // resolves modules
        resolve: {
            root:  path.join(_path, 'node_modules'),
            extensions: ['', '.js', '.ts'],
            modulesDirectories: ['node_modules']
        },

        // modules resolvers
        module: {
            loaders: [
                {
                    test: /\.styl$/,
                    loader: TextPlugin.extract('style-loader!css-loader') },
                {
                    test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                    loader : 'file?name=/fonts/[name].[ext]'},
                {
                    test: /\.css$/,
                    loader: extractCSS.extract("style-loader", "css-loader")
                },
                {
                    test: /\.(jpe?g|png|gif)$/i,
                    loader : 'file?name=/img/[name].[ext]'
                },
                {
                    test: /\.ts$/,
                    loader: "ts-loader",
                    exclude: /node_modules/
                }

            ]
        },


        // load plugins
        plugins: [
            extractCSS,
            new HtmlPlugin({
                title: 'login',
                filename: 'login.html',
                chunks: ['login'],
                template: path.join(_path,'src','view_template','login-template.html')
            }),
        ]
    };
};