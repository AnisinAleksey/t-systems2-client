package com.tsystems.logiweb.driver.util;

import java.util.HashMap;
import java.util.Map;

public class HttpRequestParameterBuilder {

    private Map<String, String> params = new HashMap<>();

    public static HttpRequestParameterBuilder create() {
        return new HttpRequestParameterBuilder();
    }

    public HttpRequestParameterBuilder add(String name, String value) {
        params.put(name, value);
        return this;
    }

    public Map<String,String> build() {
        return params;
    }


}
