package com.tsystems.logiweb.driver.service.impl;

import com.tsystems.logiweb.driver.auth.AuthenticationBean;
import com.tsystems.logiweb.driver.exception.LoginException;
import com.tsystems.logiweb.driver.service.HttpService;
import com.tsystems.logiweb.driver.service.LoginService;
import com.tsystems.logiweb.driver.util.HttpRequestParameterBuilder;
import org.apache.http.HttpResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;


@Stateless
public class LoginServiceImpl implements LoginService {

    @Inject
    private AuthenticationBean authenticationBean;

    @Inject
    private HttpService httpService;

    public void login(String username, String password) throws Exception {
        HttpResponse response = httpService.post("/login",
                HttpRequestParameterBuilder.create()
                        .add("username", username)
                        .add("password", password)
                        .build()
        );
        if (response.getStatusLine().getStatusCode() > 400)
            throw new LoginException(response.getStatusLine().getReasonPhrase());
    }

    public void logout() {
        authenticationBean.clearSession();
    }
}
