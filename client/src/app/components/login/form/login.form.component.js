"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var login_service_1 = require("../../../service/login-service");
var login_model_1 = require("../../../model/login-model");
var login_form_component_html_1 = require("./login.form.component.html");
var login_form_component_css_1 = require("./login.form.component.css");
var LoginForm = (function () {
    function LoginForm(loginService) {
        this.loginService = loginService;
        this.loginModel = new login_model_1.LoginModel();
    }
    LoginForm.prototype.login = function () {
        this.loginService.login(this.loginModel.username, this.loginModel.password);
    };
    LoginForm = __decorate([
        core_1.Component({
            selector: "login-form",
            template: login_form_component_html_1.default,
            styles: [login_form_component_css_1.default]
        }),
        __param(0, core_1.Inject(login_service_1.LoginService)), 
        __metadata('design:paramtypes', [Object])
    ], LoginForm);
    return LoginForm;
}());
exports.LoginForm = LoginForm;
//# sourceMappingURL=login.form.component.js.map