package com.tsystems.logiweb.driver.config;

import com.tsystems.logiweb.driver.controller.LoginController;
import com.tsystems.logiweb.driver.controller.MainController;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class AppConfig extends Application {
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>() {{
            add(MainController.class);
            add(LoginController.class);
        }};
    }
}
    