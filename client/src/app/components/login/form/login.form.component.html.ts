export default
`<div class = "loginForm">
    <form autocomplete="off">
        <span class = "headerSpan">Sign in</span>
        <br/>
        <span id = "hiddenSpan" class = "hiddenSpan">Incorrect login or password</span>
        <div class="fieldDiv">
            <input [(ngModel)]="loginModel.username" type="text" class="loginField" placeholder="username"/>
        </div>
        <div class="fieldDiv">
            <input [(ngModel)]="loginModel.password" type="password" class="loginField" placeholder="password"/>
        </div>
        <button class="loginButtonClass" type="submit" (click)="login()">Sign in</button>
    </form>
</div>`;