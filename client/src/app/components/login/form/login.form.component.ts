import {Component, Inject} from "@angular/core"
import {LoginService} from "../../../service/login-service"
import {LoginModel} from "../../../model/login-model";
import template from "./login.form.component.html"
import style from "./login.form.component.css"

@Component({
    selector: "login-form",
    template: template,
    styles: [style]
})
export class LoginForm {

    loginModel: LoginModel = new LoginModel();

    constructor(@Inject(LoginService) public loginService) {}

    login() {
        this.loginService.login(this.loginModel.username,this.loginModel.password);
    }
}