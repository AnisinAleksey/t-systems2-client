package com.tsystems.logiweb.driver.controller;

import com.tsystems.logiweb.driver.interceptor.ExceptionControllerHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/")
//@ExceptionControllerHandler
public class MainController extends ApplicationController {

    @GET
    public Response mainPage(@Context HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(servletContext.getContextPath() + "/login");
        return Response.ok().build();
    }
}
