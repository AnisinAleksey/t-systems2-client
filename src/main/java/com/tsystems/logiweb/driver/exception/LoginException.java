package com.tsystems.logiweb.driver.exception;

public class LoginException extends RuntimeException {

    private static final String MESSAGE = "Login failed. Reason: %s";

    public LoginException(String message) {
        super(String.format(MESSAGE, message));
    }
}
