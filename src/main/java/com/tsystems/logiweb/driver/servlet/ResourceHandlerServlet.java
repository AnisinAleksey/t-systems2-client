package com.tsystems.logiweb.driver.servlet;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ResourceHandlerServlet", urlPatterns = {"*.css", "*.js", "*.eot","*.svg","*.ttf","*.woff", "*.woff2"})
public class ResourceHandlerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        IOUtils.copy(request.getServletContext().getResourceAsStream(request.getServletPath()),response.getOutputStream());
    }
}
