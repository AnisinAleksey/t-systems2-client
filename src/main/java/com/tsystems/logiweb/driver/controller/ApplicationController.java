package com.tsystems.logiweb.driver.controller;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;

class ApplicationController {

    @Context
    protected ServletContext servletContext;

    StreamingOutput resource(final String path) {
        return new StreamingOutput() {
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                IOUtils.copy(servletContext.getResourceAsStream(path),outputStream);
            }
        };
    }
}
