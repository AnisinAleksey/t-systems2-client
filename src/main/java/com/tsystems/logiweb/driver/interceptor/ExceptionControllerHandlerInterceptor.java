package com.tsystems.logiweb.driver.interceptor;

import com.tsystems.logiweb.driver.util.JsonBuilderUtil;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Response;

@Interceptor
@ExceptionControllerHandler
public class ExceptionControllerHandlerInterceptor {

    @AroundInvoke
    public Object handle(InvocationContext ic) throws Exception {
        try {
            return ic.proceed();
        } catch (Exception e) {
            JsonBuilderUtil.create().add("message", e.getMessage());
            return Response.serverError().entity(JsonBuilderUtil.create().add("message", e.getMessage()).build());
        }
    }
}
