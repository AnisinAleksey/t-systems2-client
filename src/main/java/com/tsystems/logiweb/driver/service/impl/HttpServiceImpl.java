package com.tsystems.logiweb.driver.service.impl;

import com.tsystems.logiweb.driver.auth.AuthenticationBean;
import com.tsystems.logiweb.driver.constant.AppConstants;
import com.tsystems.logiweb.driver.service.HttpService;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Objects;

@Stateless
public class HttpServiceImpl implements HttpService {

    @Inject
    private AuthenticationBean authenticationBean;

    @Override
    public HttpResponse get(String url) throws IOException {
        HttpGet request = new HttpGet(url);
        return execute(request);
    }

    @Override
    public HttpResponse post(String url,  Map<String,String> params) throws IOException, URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(getHostUri()).setPath(url);
        params.entrySet().forEach(entry -> uriBuilder.addParameter(entry.getKey(),entry.getValue()));
        HttpPost request = new HttpPost(uriBuilder.build());
        return execute(request);
    }

    @Override
    public HttpResponse put(String url,  Map<String,String> params) throws IOException {
        HttpPut request = new HttpPut(url);
        return execute(request);
    }

    @Override
    public HttpResponse delete(String url) throws IOException {
        HttpDelete request = new HttpDelete(url);
        return execute(request);
    }

    private HttpResponse execute(HttpRequest request) throws IOException {
        HttpHost httpHost = new HttpHost("http://localhost:8080/login");
        CookieStore cookieStore = new BasicCookieStore();
        if(authenticationBean.getJSessionID() != null) {
            BasicClientCookie cookie = new BasicClientCookie(AppConstants.JSESSIONID, authenticationBean.getJSessionID());
            cookie.setPath("/");
            cookieStore.addCookie(cookie);
        }
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpClientContext localContext = HttpClientContext.create();
        localContext.setCookieStore(cookieStore);
        HttpResponse response = httpClient.execute(httpHost,request,localContext);
        Cookie jSessionId = cookieStore.getCookies().stream().filter(c -> Objects.equals(c.getName(),AppConstants.JSESSIONID)).findFirst().orElse(null);
        if(jSessionId != null)
            authenticationBean.setJSessionID(jSessionId.getValue());
        return response;
    }

    private URI getHostUri() throws URISyntaxException {
        return new URIBuilder().setHost(AppConstants.EXTERNAL_HOST).setPort(AppConstants.EXTERNAL_PORT).build();
    }
}
