package com.tsystems.logiweb.driver.service;

public interface LoginService {
    void login(String username, String password) throws Exception;
    void logout();
}
