package com.tsystems.logiweb.driver.util;

public class JsonBuilderUtil {

    private StringBuilder json = new StringBuilder("{");

    public static JsonBuilderUtil create() {
        return new JsonBuilderUtil();
    }

    public JsonBuilderUtil add(String name, Object value) {
        json.append("\"").append(name).append("\"")
                .append(":")
                .append("\"").append(value).append("\",");
        return this;
    }

    public String build() {
        if(json.charAt(json.length() - 1) == ',')
            json.deleteCharAt(json.length() - 1);
        json.append("}");
        return json.toString();
    }
}
