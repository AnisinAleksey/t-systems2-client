package com.tsystems.logiweb.driver.service;

import org.apache.http.HttpResponse;
import org.apache.http.cookie.Cookie;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

public interface HttpService {
    HttpResponse get(String url) throws IOException;
    HttpResponse post(String url, Map<String,String> params) throws IOException, URISyntaxException;
    HttpResponse put(String url,  Map<String,String> params) throws IOException;
    HttpResponse delete(String url) throws IOException;
}
