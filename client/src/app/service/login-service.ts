import {Injectable} from  "@angular/core";
import { Http, Headers, RequestOptions  } from '@angular/http';

@Injectable()
export class LoginService {

    constructor (private http: Http) {}

    login(username, password) {
        let body = JSON.stringify({'username': username, 'password': password});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        this.http.post("login", body, options).subscribe(
            err => this.handleError(err));
    }

    private handleError (err) {
        console.error(err); // log to console instead
    }
}