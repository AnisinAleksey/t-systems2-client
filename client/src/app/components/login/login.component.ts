import {Component} from "@angular/core"
import {LoginForm} from "./form/login.form.component"
import template from "./login.component.html"
import style from "./login.component.css"

@Component({
    selector: "login",
    moduleId: module.id,
    directives: [LoginForm],
    template: template,
    styles: [style]
})
export class Login {

}